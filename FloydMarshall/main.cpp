#include <iostream>

using namespace std;
const long maxV=1000;
long GR[maxV][maxV];
long nextArr[maxV][maxV];

void getShortestPath(long D[][maxV], long next[][maxV], int from, int to) {
    if (D[from][to] == INT32_MAX) {
        cout << "No path";
        return;
    }

    long c = from;
    while (c != to) {
        cout << c;
        c = next[c][to];
    }
    cout << c;
}

void printGraph(long D[][maxV], int V) {
    for (int i = 0; i < V; i++) {
        for (int j = 0; j < V; j++) {
            D[i][j] != INT32_MAX ? printf("%3ld", D[i][j]) : printf("INF");
            cout << " ";
        }
        cout << "\n";
    }
    cout << "\n";
}

//алгоритм Флойда-Уоршелла
void FU(long D[][maxV], long next[][maxV], int V) {
    int k;
    // Set zero on diagonals
    for (int i = 0; i < V; i++) {
        D[i][i] = 0;
    }

    // Next matrix init
    for (int i = 0; i < V; i++) {
        for (int j = 0; j < V; j++) {
            if (D[i][j] != INT32_MAX || i == j) {
                next[i][j] = j;
            } else {
                next[i][j] = maxV;
            }
        }
    }

    printGraph(D, V);
    printGraph(next, V);

    for (k = 0; k < V; k++) {
        for (int i = 0; i < V; i++) {
            for (int j = 0; j < V; j++) {
//                D[i][j] = min(D[i][j], D[i][k] + D[k][j]);
                if (D[i][k] + D[k][j] < D[i][j]) {
                    D[i][j] = D[i][k] + D[k][j];
                    next[i][j] = next[i][k];
                    cout << next[i][j] << " " << next[k][j] << "\n";
                }
            }
        }
    }

    printGraph(D, V);
    printGraph(next, V);
    getShortestPath(D, next, 0, 3);
}

//главная функция
int main()
{
    int n = 0;
    setlocale(LC_ALL, "Rus");
    cout << "Количество вершин в графе > ";
    cin >> n;
    cout << "Введите матрицу весов ребер:\n";
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << "GR[" << i + 1 << "][" << j + 1 << "] > ";
            int input = 0;
            cin >> input;
            if (input == 0) {
                GR[i][j] = INT32_MAX;
            } else {
                GR[i][j] = input;
            }
        }
    }
    for (int i = 0; i < maxV; i++) {
        for (int j = 0; j < maxV; j++) {
            nextArr[i][j] = -1;
        }
    }
    cout << "Матрица кратчайших путей:\n";
    FU(GR, nextArr, n);
    return 0;
}

// 4 0 1 6 0 0 0 4 1 0 0 0 0 0 0 1 0