/*
 * Дан базовый интерфейс для представления ориентированного графа:
struct IGraph {
virtual ~IGraph() {}

	// Добавление ребра от from к to.
virtual void AddEdge(int from, int to) = 0;

	virtual int VerticesCount() const  = 0;

virtual std::vector<int> GetNextVertices(int vertex) const = 0;
virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

Необходимо написать несколько реализаций интерфейса:
ListGraph, хранящий граф в виде массива списков смежности,
MatrixGraph, хранящий граф в виде матрицы смежности,
SetGraph, хранящий граф в виде массива хэш-таблиц/сбалансированных деревьев поиска,
ArcGraph, хранящий граф в виде одного массива пар {from, to}.
Также необходимо реализовать конструктор, принимающий const IGraph&. Такой конструктор должен скопировать переданный граф в создаваемый объект.
Для каждого класса создавайте отдельные h и cpp файлы.
Число вершин графа задается в конструкторе каждой реализации.
 */

#include <iostream>
#include <vector>
#include <queue>
#include <functional>

#include "ListGraph.h"
#include "MatrixGraph.h"
#include "ArcGraph.h"
#include "SetGraph.h"

// standart BFS for graphs
void BFS(const IGraph &graph, int vertex, std::function<void(int)> visit) {
    std::vector<bool> visited(graph.VertexCount(), false);
    std::queue<int> bfsQueue;
    bfsQueue.push(vertex);
    visited[vertex] = true;
    while (bfsQueue.size() > 0) {
        int current = bfsQueue.front();
        bfsQueue.pop();
        visit(current);
        std::vector<int> list = graph.GetNextVertices(current);
        for (int i = 0; i < list.size(); ++i) {
            if (!visited[list[i]]) {
                bfsQueue.push(list[i]);
                visited[list[i]] = true;
            }
        }
    }
}

// Edge struct
struct Edge {
    int from;
    int to;
};

// Presets of graphs
#define DATA1_SIZE 5
std::vector<Edge> data1 = {{0, 1},
                           {0, 2},
                           {3, 3},
                           {0, 3},
                           {1, 3},
                           {3, 4}};

#define DATA2_SIZE 4
std::vector<Edge> data2 = {{0, 1},
                           {0, 2},
                           {1, 2},
                           {2, 3},
                           {1, 3}};

#define DATA3_SIZE 5
std::vector<Edge> data3 = {{0, 1},
                           {0, 2},
                           {0, 3},
                           {1, 2},
                           {2, 3},
                           {3, 4},
                           {4, 3},
                           {4, 2}};

#define DATA4_SIZE 8
std::vector<Edge> data4 = {{0, 1},
                           {0, 2},
                           {1, 6},
                           {1, 7},
                           {2, 6},
                           {3, 1},
                           {4, 2},
                           {4, 5}};

// filling graph with edges
void fillGraphWithEdges(IGraph &graph, std::vector<Edge> data) {
    for (auto &i : data) {
        graph.AddEdge(i.from, i.to);
    }
}

// compare elements in vector by values in it
bool compareTwoVector(std::vector<int> vec1, std::vector<int> vec2) {
    if (vec1.size() != vec2.size()) {
        return false;
    }
    bool elementFound = false;
    for (int i = 0; i < vec1.size(); ++i) {
        for (int j = 0; i < vec2.size(); ++j) {
            if (vec1[i] == vec2[j]) {
                elementFound = true;
                break;
            }
        }
        if (!elementFound) {
            return false;
        }
    }
    return true;
}

// compare two graphs by BFS results
bool compareGraphsByBFS(IGraph &graph1, IGraph &graph2) {
    std::vector<int> graph1Out;
    std::vector<int> graph2Out;
    BFS(graph1, 0, [&graph1Out](int vertex) mutable { graph1Out.push_back(vertex); });
    BFS(graph2, 0, [&graph2Out](int vertex) mutable { graph2Out.push_back(vertex); });
    return compareTwoVector(graph1Out, graph2Out);
}

// Fill graphs by add by data and compare they
bool fillByAddAndCompare(IGraph &graph1, IGraph &graph2, std::vector<Edge> &data) {
    fillGraphWithEdges(graph1, data);

    fillGraphWithEdges(graph2, data);

    return compareGraphsByBFS(graph1, graph2);
}


void addCreationBFSTest() {
    std::cout << "TEST RUNNING...\n";
    std::cout << "ADD CREATION BFS LIST GRAPH, MATRIX GRAPH\n";
    std::cout << "TEST Graphs: ";
    {
        ListGraph graph1(DATA1_SIZE);
        MatrixGraph graph2(DATA1_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data1)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST BFS: ";

    {
        ListGraph graph1(DATA2_SIZE);
        MatrixGraph graph2(DATA2_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST Dijkstra: ";

    {
        ListGraph graph1(DATA3_SIZE);
        MatrixGraph graph2(DATA3_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data3)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST A-Star: ";

    {
        ListGraph graph1(DATA4_SIZE);
        MatrixGraph graph2(DATA4_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data4)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n\nADD CREATION BFS LIST GRAPH, ARC GRAPH\n";
    std::cout << "TEST Graphs: ";
    {
        ListGraph graph1(DATA1_SIZE);
        ArcGraph graph2(DATA1_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data1)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST BFS: ";
    {
        ListGraph graph1(DATA2_SIZE);
        ArcGraph graph2(DATA2_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST Dijkstra: ";
    {
        ListGraph graph1(DATA3_SIZE);
        ArcGraph graph2(DATA3_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data3)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST A-Star: ";
    {
        ListGraph graph1(DATA4_SIZE);
        ArcGraph graph2(DATA4_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data4)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }
    ////////////////////////////////////F///////////////////////////////////////////////////////////////////////////////
    std::cout << "\n\nADD CREATION BFS LIST GRAPH, SET GRAPH\n";
    std::cout << "TEST Graphs: ";
    {
        SetGraph graph1(DATA1_SIZE);
        ListGraph graph2(DATA1_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data1)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST BFS: ";
    {
        SetGraph graph1(DATA2_SIZE);
        ListGraph graph2(DATA2_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST Dijkstra: ";
    {
        SetGraph graph1(DATA3_SIZE);
        ListGraph graph2(DATA3_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data3)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST A-Star: ";
    {
        SetGraph graph1(DATA4_SIZE);
        ListGraph graph2(DATA4_SIZE);

        if (fillByAddAndCompare(graph1, graph2, data4)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "___________________________________________________________________________________________________";
}

void creationFromGraphTestBFS() {
    std::cout << "\n\nTEST RUNNING...\n";
    std::cout << "CREATION FROM GRAPH BFS LIST GRAPH, MATRIX GRAPH\n";
    std::cout << "TEST Graphs: ";
    {
        ListGraph graph1(DATA1_SIZE);
        fillGraphWithEdges(graph1, data1);
        MatrixGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST BFS: ";
    {
        ListGraph graph1(DATA2_SIZE);
        fillGraphWithEdges(graph1, data2);
        MatrixGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST Dijkstra: ";
    {
        ListGraph graph1(DATA3_SIZE);
        fillGraphWithEdges(graph1, data3);
        MatrixGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST A-Star: ";
    {
        ListGraph graph1(DATA4_SIZE);
        fillGraphWithEdges(graph1, data4);
        MatrixGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    std::cout << "\n\nCREATION FROM GRAPH BFS LIST GRAPH, ARC GRAPH\n";
    std::cout << "TEST Graphs: ";
    {
        ListGraph graph1(DATA1_SIZE);
        fillGraphWithEdges(graph1, data1);
        ArcGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST BFS: ";
    {
        ListGraph graph1(DATA2_SIZE);
        fillGraphWithEdges(graph1, data2);
        ArcGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST Dijkstra: ";
    {
        ListGraph graph1(DATA3_SIZE);
        fillGraphWithEdges(graph1, data3);
        ArcGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST A-Star: ";
    {
        ListGraph graph1(DATA4_SIZE);
        fillGraphWithEdges(graph1, data4);
        ArcGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    std::cout << "\n\nCREATION FROM GRAPH BFS LIST GRAPH, SET GRAPH\n";
    std::cout << "TEST Graphs: ";
    {
        ListGraph graph1(DATA1_SIZE);
        fillGraphWithEdges(graph1, data1);
        SetGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST BFS: ";
    {
        ListGraph graph1(DATA2_SIZE);
        fillGraphWithEdges(graph1, data2);
        SetGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST Dijkstra: ";
    {
        ListGraph graph1(DATA3_SIZE);
        fillGraphWithEdges(graph1, data3);
        SetGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "TEST A-Star: ";
    {
        ListGraph graph1(DATA4_SIZE);
        fillGraphWithEdges(graph1, data4);
        SetGraph graph2(graph1);
        if (compareGraphsByBFS(graph1, graph2)) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }

    std::cout << "___________________________________________________________________________________________________";
}

void prevVerticesTest(IGraph& graph1, IGraph& graph2) {
    for (int i = 0; i < graph1.VertexCount(); ++i) {
        std::vector<int> list1 = graph1.GetPrevVertices(i);
        std::vector<int> list2 = graph2.GetPrevVertices(i);
        if (!compareTwoVector(list1, list2)) {
            std::cout << "FAIL\n";
            return;
        }
    }
    std::cout << "OK\n";
}

void prevVerticesTestImpl() {
    std::cout << "\n\nTEST RUNNING...\n";
    std::cout << "PREV VERTICES LIST, MATRIX GRAPHS TEST\n";
    std::cout << "TEST Graphs: ";
    {
        MatrixGraph graph1 = MatrixGraph(DATA1_SIZE);
        fillGraphWithEdges(graph1, data1);
        ListGraph graph2 = ListGraph(DATA1_SIZE);
        fillGraphWithEdges(graph2, data1);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST BFS: ";
    {
        MatrixGraph graph1 = MatrixGraph(DATA2_SIZE);
        fillGraphWithEdges(graph1, data2);
        ListGraph graph2 = ListGraph(DATA2_SIZE);
        fillGraphWithEdges(graph2, data2);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST Dijkstra: ";
    {
        MatrixGraph graph1 = MatrixGraph(DATA3_SIZE);
        fillGraphWithEdges(graph1, data1);
        ListGraph graph2 = ListGraph(DATA3_SIZE);
        fillGraphWithEdges(graph2, data1);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST A-Star: ";
    {
        MatrixGraph graph1 = MatrixGraph(DATA4_SIZE);
        fillGraphWithEdges(graph1, data4);
        ListGraph graph2 = ListGraph(DATA4_SIZE);
        fillGraphWithEdges(graph2, data4);
        prevVerticesTest(graph1, graph2);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n\nPREV VERTICES LIST, ARC GRAPHS TEST\n";
    std::cout << "TEST Graphs: ";
    {
        ArcGraph graph1 = ArcGraph(DATA1_SIZE);
        fillGraphWithEdges(graph1, data1);
        ListGraph graph2 = ListGraph(DATA1_SIZE);
        fillGraphWithEdges(graph2, data1);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST BFS: ";
    {
        ArcGraph graph1 = ArcGraph(DATA2_SIZE);
        fillGraphWithEdges(graph1, data2);
        ListGraph graph2 = ListGraph(DATA2_SIZE);
        fillGraphWithEdges(graph2, data2);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST Dijkstra: ";
    {
        ArcGraph graph1 = ArcGraph(DATA3_SIZE);
        fillGraphWithEdges(graph1, data3);
        ListGraph graph2 = ListGraph(DATA3_SIZE);
        fillGraphWithEdges(graph2, data3);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST A-Star: ";
    {
        ArcGraph graph1 = ArcGraph(DATA4_SIZE);
        fillGraphWithEdges(graph1, data4);
        ListGraph graph2 = ListGraph(DATA4_SIZE);
        fillGraphWithEdges(graph2, data4);
        prevVerticesTest(graph1, graph2);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n\nPREV VERTICES LIST, SET GRAPHS TEST\n";
    std::cout << "TEST Graphs: ";
    {
        SetGraph graph1 = SetGraph(DATA1_SIZE);
        fillGraphWithEdges(graph1, data1);
        ListGraph graph2 = ListGraph(DATA1_SIZE);
        fillGraphWithEdges(graph2, data1);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST BFS: ";
    {
        SetGraph graph1 = SetGraph(DATA2_SIZE);
        fillGraphWithEdges(graph1, data2);
        ListGraph graph2 = ListGraph(DATA2_SIZE);
        fillGraphWithEdges(graph2, data2);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST Dijkstra: ";
    {
        SetGraph graph1 = SetGraph(DATA3_SIZE);
        fillGraphWithEdges(graph1, data3);
        ListGraph graph2 = ListGraph(DATA3_SIZE);
        fillGraphWithEdges(graph2, data3);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "TEST A-Star: ";
    {
        SetGraph graph1 = SetGraph(DATA4_SIZE);
        fillGraphWithEdges(graph1, data4);
        ListGraph graph2 = ListGraph(DATA4_SIZE);
        fillGraphWithEdges(graph2, data4);
        prevVerticesTest(graph1, graph2);
    }

    std::cout << "___________________________________________________________________________________________________";
}

int main() {
    // create graphs by add functions and compare them by BFS
    addCreationBFSTest();
    // create graph by other graph and compare them by BFS
    creationFromGraphTestBFS();
    // create graphs and compare their prev vertices for every vertex
    prevVerticesTestImpl();
    return 0;
}
