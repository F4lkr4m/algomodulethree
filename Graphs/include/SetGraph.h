#ifndef ALGO_3_MOD_SETGRAPH_H
#define ALGO_3_MOD_SETGRAPH_H

#include <set>

#include "IGraph.h"

class SetGraph: public IGraph {
public:
    explicit SetGraph(int vertexCount);
    explicit SetGraph(IGraph& graph);

    void AddEdge(int from, int to) override;

    int VertexCount() const  override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;
private:
    std::vector<std::set<int>> sets;
};


#endif //ALGO_3_MOD_SETGRAPH_H
