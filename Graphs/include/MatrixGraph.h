#ifndef ALGO_3_MOD_MATRIXGRAPH_H
#define ALGO_3_MOD_MATRIXGRAPH_H

#include "IGraph.h"

class MatrixGraph: public IGraph {
public:
    explicit MatrixGraph(int vertexCount);
    explicit MatrixGraph(IGraph& graph);

    void AddEdge(int from, int to) override;

    int VertexCount() const  override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<int>> matrix;
};


#endif //ALGO_3_MOD_MATRIXGRAPH_H
