#ifndef ALGO_3_MOD_ARCGRAPH_H
#define ALGO_3_MOD_ARCGRAPH_H

#include "IGraph.h"

class ArcGraph: public IGraph {
public:
    struct FromTo {
        int from;
        int to;
        FromTo(int from, int to): from(from), to(to) {}
    };

    explicit ArcGraph(int verticesCount);
    explicit ArcGraph(IGraph& graph);

    void AddEdge(int from, int to) override;

    int VertexCount() const  override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<FromTo> array;
    int verticesCount;
};


#endif //ALGO_3_MOD_ARCGRAPH_H
