#ifndef ALGO_3_MOD_LISTGRAPH_H
#define ALGO_3_MOD_LISTGRAPH_H

#include "IGraph.h"

class ListGraph: public IGraph {
public:
    explicit ListGraph(int vertexCount);
    explicit ListGraph(IGraph& graph);

    void AddEdge(int from, int to) override;

    int VertexCount() const  override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<int>> lists;
};

#endif //ALGO_3_MOD_LISTGRAPH_H
