#ifndef ALGO_3_MOD_IGRAPH_H
#define ALGO_3_MOD_IGRAPH_H

#include <vector>

class IGraph {
public:
    virtual ~IGraph() {}

    // Добавление ребра от from к to.
    virtual void AddEdge(int from, int to) = 0;

    virtual int VertexCount() const  = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

#endif //ALGO_3_MOD_IGRAPH_H
