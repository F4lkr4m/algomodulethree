#include "MatrixGraph.h"
#include <cassert>

MatrixGraph::MatrixGraph(int vertexCount): matrix(vertexCount) {
    assert(vertexCount > 0);
    for (int i = 0; i < vertexCount; ++i) {
        matrix[i] = std::vector<int>(vertexCount);
    }
}

MatrixGraph::MatrixGraph(IGraph &graph): matrix(graph.VertexCount()) {
    for (int i = 0; i < graph.VertexCount(); ++i) {
        matrix[i] = std::vector<int>(graph.VertexCount());
    }

    std::vector<int> list;
    for (int i = 0; i < graph.VertexCount(); ++i) {
        list = graph.GetNextVertices(i);
        for (int j = 0; j < list.size(); ++j) {
            matrix[i][list[j]] += 1;
        }
    }
}

void MatrixGraph::AddEdge(int from, int to) {
    matrix[from][to] += 1;
}

int MatrixGraph::VertexCount() const {
    return matrix.size();
}

std::vector<int> MatrixGraph::GetNextVertices(int vertex) const {
    assert(vertex >= 0 && vertex < matrix.size());
    std::vector<int> list;
    for (int i = 0; i < matrix.size(); ++i) {
        if (matrix[vertex][i] == 1) {
            list.push_back(i);
        }
    }
    return list;
}

std::vector<int> MatrixGraph::GetPrevVertices(int vertex) const {
    assert(vertex >= 0 && vertex < matrix.size());

    std::vector<int> list;
    for (int i = 0; i < matrix.size(); ++i) {
        if (matrix[i][vertex] > 0) {
            list.push_back(i);
        }
    }
    return list;
}
