#include "SetGraph.h"

SetGraph::SetGraph(int vertexCount): sets(vertexCount) {

}

SetGraph::SetGraph(IGraph& graph): sets(graph.VertexCount()) {
    std::vector<int> list;
    for (int i = 0; i < sets.size(); ++i) {
        list = graph.GetNextVertices(i);
        for (int & j : list) {
            sets[i].insert(j);
        }
    }
}

void SetGraph::AddEdge(int from, int to) {
    sets[from].insert(to);
}

int SetGraph::VertexCount() const {
    return sets.size();
}

std::vector<int> SetGraph::GetNextVertices(int vertex) const {
    std::vector<int> list;
    for (auto& el: sets[vertex]) {
        list.push_back(el);
    }
    return list;
}

std::vector<int> SetGraph::GetPrevVertices(int vertex) const {
    std::vector<int> list;
    for (int i = 0; i < sets.size(); ++i) {
        if (sets[i].contains(vertex)) {
            list.push_back(i);
        }
    }
    return list;
}