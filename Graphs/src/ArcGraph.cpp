#include "ArcGraph.h"

ArcGraph::ArcGraph(int verticesCount):verticesCount(verticesCount) {

}

ArcGraph::ArcGraph(IGraph& graph) {
    verticesCount = graph.VertexCount();
    std::vector<int> list;
    for (int i = 0; i < graph.VertexCount(); ++i) {
        list = graph.GetNextVertices(i);
        for (int j = 0; j < list.size(); ++j) {
            array.push_back(FromTo(i, list[j]));
        }
    }
}

void ArcGraph::AddEdge(int from, int to) {
    array.push_back((FromTo(from, to)));
}

int ArcGraph::VertexCount() const {
    return verticesCount;
}

std::vector<int> ArcGraph::GetNextVertices(int vertex) const {
    std::vector<int> list;
    for (int i = 0; i < array.size(); ++i) {
        if (array[i].from == vertex) {
            list.push_back(array[i].to);
        }
    }
    return list;
}

std::vector<int> ArcGraph::GetPrevVertices(int vertex) const {
    std::vector<int> list;
    for (int i = 0; i < array.size(); ++i) {
        if (array[i].to == vertex) {
            list.push_back(array[i].from);
        }
    }
    return list;
}
