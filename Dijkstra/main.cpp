/*
 * Требуется отыскать самый выгодный маршрут между городами.
Требования: время работы O((N+M)logN), где N-количество городов, M-известных дорог между ними.
 Формат входных данных.
Первая строка содержит число N – количество городов.
Вторая строка содержит число M - количество дорог.
Каждая следующая строка содержит описание дороги (откуда, куда, время в пути).
Последняя строка содержит маршрут (откуда и куда нужно доехать).
Формат выходных данных.
Вывести длину самого выгодного маршрута.
 */

#include <iostream>
#include <vector>
#include <set>
#include <cassert>

void test1();

void test2();

void test3();

void test4();

class IGraph {
public:
    virtual ~IGraph() {}

    virtual void AddEdge(int from, int to, int len) = 0;

    virtual int VertexCount() const = 0;

    struct ToLen {
        int to;
        int len;

        ToLen(int to, int len) : to(to), len(len) {}
    };

    virtual std::vector<ToLen> GetNextVertices(int vertex) const = 0;

    virtual std::vector<ToLen> GetPrevVertices(int vertex) const = 0;
};

class WeighedListGraph : public IGraph {
public:
    explicit WeighedListGraph(int vertexCount);

    explicit WeighedListGraph(IGraph &graph);

    void AddEdge(int from, int to, int len) override;

    int VertexCount() const override;

    std::vector<ToLen> GetNextVertices(int vertex) const override;

    std::vector<ToLen> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<ToLen>> lists;
};

WeighedListGraph::WeighedListGraph(int vertexCount) : lists(vertexCount) {

}

WeighedListGraph::WeighedListGraph(IGraph &graph) : lists(graph.VertexCount()) {
    for (int i = 0; i < lists.size(); ++i) {
        lists[i] = graph.GetNextVertices(i);
    }
}

void WeighedListGraph::AddEdge(int from, int to, int len) {
    assert(from >= 0 && from < lists.size());
    assert(to >= 0 && to < lists.size());

    lists[from].push_back(ToLen(to, len));
    lists[to].push_back(ToLen(from, len));
}

int WeighedListGraph::VertexCount() const {
    return lists.size();
}

std::vector<IGraph::ToLen> WeighedListGraph::GetNextVertices(int vertex) const {
    assert(vertex >= 0 && vertex < lists.size());
    return lists[vertex];
}

std::vector<IGraph::ToLen> WeighedListGraph::GetPrevVertices(int vertex) const {
    assert(vertex >= 0 && vertex < lists.size());
    std::vector<ToLen> result;
    for (int from = 0; from < lists.size(); ++from) {
        for (int i = 0; i < lists[from].size(); ++i) {
            if (lists[from][i].to == vertex) {
                result.push_back(lists[from][i]);
            }
        }
    }

    return result;
}

struct PairComparatorOfLen {
    bool operator()(const std::pair<int, int> lhs, const std::pair<int, int> rhs) {
        if (lhs.second == rhs.second) {
            return lhs.first < rhs.first;
        } else {
            return lhs.second < rhs.second;
        }
    }
};


bool Relax(std::vector<int> &lengths, const int fromVertex, IGraph::ToLen &vertexToLen) {
    // if way to vertex is shorter update way
    if (lengths[vertexToLen.to] > lengths[fromVertex] + vertexToLen.len) {
        int tmpLength = lengths[vertexToLen.to];
        lengths[vertexToLen.to] = lengths[fromVertex] + vertexToLen.len;

        // set old value to erase old one
        vertexToLen.len = tmpLength;

        return true;
    }
    return false;
}

int findTheShortestWayByDijkstra(const IGraph &graph, const int from, const int to) {
    // vector with lengths to vertices
    std::vector<int> lengths(graph.VertexCount());
    lengths.assign(lengths.size(), INT32_MAX);
    // mark begin vertex as visited
    lengths[from] = 0;

    // pair of <point, len to this point>
    std::set<std::pair<int, int>, PairComparatorOfLen> set;
    set.insert(std::pair<int, int>(from, lengths[from]));

    while (!set.empty()) {
        // get the first element from set
        auto vertex = set.begin();

        std::vector<IGraph::ToLen> listVertexToLen = graph.GetNextVertices(vertex->first);
        for (int i = 0; i < listVertexToLen.size(); ++i) {
            if (lengths[listVertexToLen[i].to] == INT32_MAX) {
                // if vertex first visited
                lengths[listVertexToLen[i].to] = lengths[vertex->first] + listVertexToLen[i].len;
                set.insert(std::pair<int, int>(listVertexToLen[i].to, lengths[listVertexToLen[i].to]));
            } else if (Relax(lengths, vertex->first, listVertexToLen[i])) {
                // Decreasing key,
                // listVertexToLen was updated in Relax() by old value in set, to find it out and erase
                set.erase(std::pair<int, int>(listVertexToLen[i].to, listVertexToLen[i].len));
                set.insert(std::pair<int, int>(listVertexToLen[i].to, lengths[listVertexToLen[i].to]));
            }
        }
        set.erase(vertex);
    }

    return lengths[to];
}

int main() {
    test1();
    test2();
    test3();
    test4();

//    int n; // number of cities
//    std::cin >> n;
//    WeighedListGraph graph = WeighedListGraph(n);
//    int m; // number of roads
//    std::cin >> m;
//    int from;
//    int to;
//    int len;
//    for (int i = 0; i < m; ++i) {
//        std::cin >> from >> to >> len;
//        graph.AddEdge(from, to, len);
//    }
//
//    std::cin >> from >> to;
//    std::cout << findTheMinimalTreeByPrim(graph, from, to);

    return 0;
}

/////////////////////////////////////////////////TESTS//////////////////////////////////////////////////////////////////

struct FromToLen {
    int from;
    int to;
    int len;

    FromToLen(int from, int to, int len) : from(from), to(to), len(len) {}
};

void test1() {
    std::vector<FromToLen> data = {{0, 3, 1},
                                   {0, 4, 2},
                                   {1, 2, 7},
                                   {1, 3, 2},
                                   {1, 4, 3},
                                   {1, 5, 3},
                                   {2, 5, 3},
                                   {3, 4, 4},
                                   {3, 5, 6}};

    int n = 6;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from, data[i].to, data[i].len);
    }
    int from = 0;
    int to = 2;
    if (int result = findTheShortestWayByDijkstra(graph, from, to) == 9) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED 9 RESULT: " << result << "\n";
    }

}

void test2() {
    std::vector<FromToLen> data = {{0, 2, 2},
                                   {0, 3, 4},
                                   {2, 3, 1},
                                   {2, 4, 7},
                                   {3, 4, 4},
                                   {3, 5, 3},
                                   {5, 4, 3},
                                   {4, 6, 2}};

    int n = 7;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from, data[i].to, data[i].len);
    }
    int from = 0;
    int to = 6;
    if (int result = findTheShortestWayByDijkstra(graph, from, to) == 9) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED 9 RESULT: " << result << "\n";
    }
}

void test3() {
    std::vector<FromToLen> data = {{0, 1, 1},
                                   {0, 2, 2},
                                   {0, 2, 1},
                                   {0, 2, 3},
                                   {0, 2, 5},
                                   {0, 3, 3},
                                   {0, 4, 4},
                                   {1, 3, 1},
                                   {2, 4, 2},
                                   {3, 5, 2},
                                   {4, 6, 1},
                                   {4, 7, 3},
                                   {3, 7, 5},
                                   {5, 7, 2},
                                   {6, 7, 1},};

    int n = 8;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from, data[i].to, data[i].len);
    }
    int from = 0;
    int to = 7;
    if (int result = findTheShortestWayByDijkstra(graph, from, to) == 5) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED Prim RESULT: " << result << "\n";
    }
}

void test4() {
    std::vector<FromToLen> data = {{0, 1, 10},
                                   {0, 2, 11},
                                   {1, 4, 23},
                                   {1, 3, 8},
                                   {1, 6, 12},
                                   {2, 3, 4},
                                   {2, 6, 18},
                                   {2, 5, 6},
                                   {3, 7, 13},
                                   {4, 6, 9},
                                   {4, 8, 15},
                                   {5, 7, 8},
                                   {6, 8, 13},};

    int n = 9;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from, data[i].to, data[i].len);
    }
    int from = 0;
    int to = 8;
    if (int result = findTheShortestWayByDijkstra(graph, from, to) == 35) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED 35 RESULT: " << result << "\n";
    }
}