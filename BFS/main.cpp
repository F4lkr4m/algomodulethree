/*
 * Дан невзвешенный неориентированный граф. В графе может быть несколько кратчайших путей между какими-то вершинами.
 * Найдите количество различных кратчайших путей между заданными вершинами.
 *
 * Требования: сложность O(V+E).
Формат ввода.
v: кол-во вершин (макс. 50000),
n: кол-во ребер (макс. 200000),
n пар реберных вершин,
пара вершин u, w для запроса.
Формат вывода.
Количество кратчайших путей от u к w.
 */


#include <iostream>
#include <vector>
#include <queue>
#include <cassert>

void test1();

void test2();

void test3();

void test4();

void test5();

void test6();

void test7();

struct IGraph {
    virtual ~IGraph() {}

    // Добавление ребра от from к to.
    virtual void AddEdge(int from, int to) = 0;

    virtual int VertexCount() const = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
};

class ListGraph : public IGraph {
public:
    ListGraph(int vertexCount);

    ListGraph(IGraph &graph);

    // Добавление ребра от from к to.
    virtual void AddEdge(int from, int to) override;

    virtual int VertexCount() const override;

    virtual std::vector<int> GetNextVertices(int vertex) const override;

private:
    std::vector<std::vector<int>> lists;
};

ListGraph::ListGraph(int vertexCount) : lists(vertexCount) {

}

ListGraph::ListGraph(IGraph &graph) : lists(graph.VertexCount()) {
    for (int i = 0; i < lists.size(); ++i) {
        lists[i] = graph.GetNextVertices(i);
    }
}

void ListGraph::AddEdge(int from, int to) {
    assert(from >= 0 && from < lists.size());
    assert(to >= 0 && to < lists.size());

    lists[from].push_back(to);
    lists[to].push_back(from);
}

int ListGraph::VertexCount() const {
    return lists.size();
}

std::vector<int> ListGraph::GetNextVertices(int vertex) const {
    assert(vertex >= 0 && vertex < lists.size());
    return lists[vertex];
}

int getTheShortestWaysToEdge(const IGraph &graph, int begin, int end) {
    std::vector<bool> visited(graph.VertexCount(), false);

    // create vector that remember number of routes to edge
    std::vector<int> numberOfRoutes(graph.VertexCount(), 0);

    // create vector for length from begin
    std::vector<int> lengthFromBegin(graph.VertexCount());
    lengthFromBegin.assign(lengthFromBegin.size(), INT32_MAX);
    lengthFromBegin[begin] = 0;

    // mark first as Graphs route has
    numberOfRoutes[begin] = 1;

    std::queue<int> bfsQueue;
    bfsQueue.push(begin);
    int theShortestWay = INT32_MAX;

    bool isElementFounded = false;
    // mark first as visited
    visited[begin] = false;
    while (!bfsQueue.empty()) {
        // get element at head of queue
        int current = bfsQueue.front();
        bfsQueue.pop();

        // get next lists
        std::vector<int> list = graph.GetNextVertices(current);
        for (int i = 0; i < list.size(); ++i) {
            if (visited[list[i]]) {
                continue;
            }

            // found end edge
            if (list[i] == end && !isElementFounded) {
                isElementFounded = true;
                if (lengthFromBegin[list[i]] > lengthFromBegin[current] + 1) {
                    lengthFromBegin[list[i]] = lengthFromBegin[current] + 1;
                }
                // the shortest way is the first
                theShortestWay = lengthFromBegin[list[i]];
            }

            // fill the shortest way
            if (lengthFromBegin[list[i]] > lengthFromBegin[current] + 1 && !isElementFounded) {
                lengthFromBegin[list[i]] = lengthFromBegin[current] + 1;
                bfsQueue.push(list[i]);
            }

            if (lengthFromBegin[list[i]] != lengthFromBegin[current] && list[i] != end) {
                numberOfRoutes[list[i]] += numberOfRoutes[current];
            }

            if (list[i] == end && (theShortestWay == lengthFromBegin[current] + 1)) {
                numberOfRoutes[end] += numberOfRoutes[current];
            }
        }
        visited[current] = true;
    }
    return numberOfRoutes[end];
}

int main() {
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();

//    int n;
//    int w;
//    std::cin >> n >> w;
//    ListGraph graph(n);
//    int from;
//    int to;
//    for (int i = 0; i < w; ++i) {
//        std::cin >> from >> to;
//        graph.AddEdge(from, to);
//    }
//    int begin;
//    int end;
//    std::cin >> begin;
//    std::cin >> end;
//    std::cout << getTheShortestWaysToEdge(graph, begin, end);

    return 0;
}

void test1() {
    ListGraph graph(4);
    graph.AddEdge(0, 1);
    graph.AddEdge(0, 2);
    graph.AddEdge(1, 2);
    graph.AddEdge(1, 3);
    graph.AddEdge(2, 3);
    int result = getTheShortestWaysToEdge(graph, 0, 3);
    if (result == 2) {
        std::cout << "TEST1: OK\n";
    } else {
        std::cout << "TEST1: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }
}

void test2() {
    ListGraph graph(7);
    graph.AddEdge(0, 1);
    graph.AddEdge(0, 6);
    graph.AddEdge(6, 5);
    graph.AddEdge(1, 5);
    graph.AddEdge(1, 2);
    graph.AddEdge(5, 2);
    graph.AddEdge(5, 4);
    graph.AddEdge(2, 3);
    graph.AddEdge(4, 3);
    int result = getTheShortestWaysToEdge(graph, 0, 3);
    if (result == 1) {
        std::cout << "TEST2: OK\n";
    } else {
        std::cout << "TEST2: EXPECTED: Graphs, RESULT: " + std::to_string(result) << "\n";
    }
    result = getTheShortestWaysToEdge(graph, 0, 4);
    if (result == 2) {
        std::cout << "TEST2: OK\n";
    } else {
        std::cout << "TEST2: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }
    result = getTheShortestWaysToEdge(graph, 6, 2);
    if (result == 1) {
        std::cout << "TEST2: OK\n";
    } else {
        std::cout << "TEST2: EXPECTED: Graphs, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 0, 5);
    if (result == 2) {
        std::cout << "TEST2: OK\n";
    } else {
        std::cout << "TEST2: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }
}

void test3() {
    ListGraph graph(7);
    graph.AddEdge(0, 1);
    graph.AddEdge(0, 2);
    graph.AddEdge(1, 2);
    graph.AddEdge(1, 3);
    graph.AddEdge(1, 4);
    graph.AddEdge(2, 4);
    graph.AddEdge(2, 5);
    graph.AddEdge(4, 5);
    graph.AddEdge(3, 4);
    graph.AddEdge(3, 6);
    graph.AddEdge(4, 6);
    graph.AddEdge(5, 6);
    int result = getTheShortestWaysToEdge(graph, 0, 6);
    if (result == 4) {
        std::cout << "TEST3: OK\n";
    } else {
        std::cout << "TEST3: EXPECTED: A-Star, RESULT: " + std::to_string(result) << "\n";
    }
    result = getTheShortestWaysToEdge(graph, 0, 4);
    if (result == 2) {
        std::cout << "TEST3: OK\n";
    } else {
        std::cout << "TEST3: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 0, 3);
    if (result == 1) {
        std::cout << "TEST3: OK\n";
    } else {
        std::cout << "TEST3: EXPECTED: Graphs, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 2, 3);
    if (result == 2) {
        std::cout << "TEST3: OK\n";
    } else {
        std::cout << "TEST3: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 6, 2);
    if (result == 2) {
        std::cout << "TEST3: OK\n";
    } else {
        std::cout << "TEST3: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }
}

void test4() {
    ListGraph graph(6);
    graph.AddEdge(0, 1);
    graph.AddEdge(0, 2);
    graph.AddEdge(1, 2);
    graph.AddEdge(1, 3);
    graph.AddEdge(3, 2);
    graph.AddEdge(3, 4);
    graph.AddEdge(2, 4);
    graph.AddEdge(4, 5);

    int result = getTheShortestWaysToEdge(graph, 0, 5);
    if (result == 1) {
        std::cout << "TEST4: OK\n";
    } else {
        std::cout << "TEST4: EXPECTED: Graphs, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 1, 4);
    if (result == 2) {
        std::cout << "TEST4: OK\n";
    } else {
        std::cout << "TEST4: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 5, 1);
    if (result == 2) {
        std::cout << "TEST4: OK\n";
    } else {
        std::cout << "TEST4: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }
}

void test5() {
    ListGraph graph(10);
    graph.AddEdge(0, 1);
    graph.AddEdge(0, 2);
    graph.AddEdge(1, 2);
    graph.AddEdge(1, 3);
    graph.AddEdge(2, 3);
    graph.AddEdge(3, 4);
    graph.AddEdge(3, 5);
    graph.AddEdge(3, 6);
    graph.AddEdge(4, 8);
    graph.AddEdge(5, 7);
    graph.AddEdge(6, 7);
    graph.AddEdge(7, 9);
    graph.AddEdge(8, 9);

    int result = getTheShortestWaysToEdge(graph, 0, 9);
    if (result == 6) {
        std::cout << "TEST5: OK\n";
    } else {
        std::cout << "TEST5: EXPECTED: 6, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 3, 9);
    if (result == 3) {
        std::cout << "TEST5: OK\n";
    } else {
        std::cout << "TEST5: EXPECTED: Dijkstra, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 5, 0);
    if (result == 2) {
        std::cout << "TEST5: OK\n";
    } else {
        std::cout << "TEST5: EXPECTED: BFS, RESULT: " + std::to_string(result) << "\n";
    }
}

void test6() {
    ListGraph graph(11);
    graph.AddEdge(0, 1);
    graph.AddEdge(0, 2);
    graph.AddEdge(0, 3);
    graph.AddEdge(0, 4);
    graph.AddEdge(1, 2);
    graph.AddEdge(2, 3);
    graph.AddEdge(3, 4);
    graph.AddEdge(1, 5);
    graph.AddEdge(2, 6);
    graph.AddEdge(6, 5);
    graph.AddEdge(3, 7);
    graph.AddEdge(6, 7);
    graph.AddEdge(5, 9);
    graph.AddEdge(6, 9);
    graph.AddEdge(7, 9);
    graph.AddEdge(4, 8);
    graph.AddEdge(8, 9);

    int result = getTheShortestWaysToEdge(graph, 0, 9);
    if (result == 4) {
        std::cout << "TEST6: OK\n";
    } else {
        std::cout << "TEST6: EXPECTED: A-Star, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 3, 9);
    if (result == 1) {
        std::cout << "TEST6: OK\n";
    } else {
        std::cout << "TEST6: EXPECTED: Graphs, RESULT: " + std::to_string(result) << "\n";
    }

    result = getTheShortestWaysToEdge(graph, 4, 6);
    if (result == 4) {
        std::cout << "TEST6: OK\n";
    } else {
        std::cout << "TEST6: EXPECTED: A-Star, RESULT: " + std::to_string(result) << "\n";
    }
}

void test7() {
    ListGraph graph(4);
    graph.AddEdge(0, 1);
    graph.AddEdge(0, 3);
    graph.AddEdge(0, 2);
    graph.AddEdge(1, 3);
    graph.AddEdge(2, 3);

    int result = getTheShortestWaysToEdge(graph, 0, 3);
    if (result == 1) {
        std::cout << "TEST7: OK\n";
    } else {
        std::cout << "TEST7: EXPECTED: Graphs, RESULT: " + std::to_string(result) << "\n";
    }
}
