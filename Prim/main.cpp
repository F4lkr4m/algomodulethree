/*
 * Вариант BFS. Для построения минимального остовного дерева используйте алгоритм Прима.
 */

#include <iostream>
#include <vector>
#include <set>
#include <cassert>

void test1();

void test2();

void test3();

void test4();

void test5();

class IGraph {
public:
    virtual ~IGraph() {}

    virtual void AddEdge(int from, int to, int len) = 0;

    virtual int VertexCount() const = 0;

    struct ToLen {
        int to;
        int len;

        ToLen(int to, int len) : to(to), len(len) {}
    };

    virtual std::vector<ToLen> GetNextVertices(int vertex) const = 0;

    virtual std::vector<ToLen> GetPrevVertices(int vertex) const = 0;
};

class WeighedListGraph : public IGraph {
public:
    explicit WeighedListGraph(int vertexCount);

    explicit WeighedListGraph(IGraph &graph);

    void AddEdge(int from, int to, int len) override;

    int VertexCount() const override;

    std::vector<ToLen> GetNextVertices(int vertex) const override;

    std::vector<ToLen> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<ToLen>> lists;
};

WeighedListGraph::WeighedListGraph(int vertexCount) : lists(vertexCount) {

}

WeighedListGraph::WeighedListGraph(IGraph &graph) : lists(graph.VertexCount()) {
    for (int i = 0; i < lists.size(); ++i) {
        lists[i] = graph.GetNextVertices(i);
    }
}

void WeighedListGraph::AddEdge(int from, int to, int len) {
    assert(from >= 0 && from < lists.size());
    assert(to >= 0 && to < lists.size());

    lists[from].push_back(ToLen(to, len));
    lists[to].push_back(ToLen(from, len));
}

int WeighedListGraph::VertexCount() const {
    return lists.size();
}

std::vector<IGraph::ToLen> WeighedListGraph::GetNextVertices(int vertex) const {
    assert(vertex >= 0 && vertex < lists.size());
    return lists[vertex];
}

std::vector<IGraph::ToLen> WeighedListGraph::GetPrevVertices(int vertex) const {
    assert(vertex >= 0 && vertex < lists.size());
    std::vector<ToLen> result;
    for (int from = 0; from < lists.size(); ++from) {
        for (int i = 0; i < lists[from].size(); ++i) {
            if (lists[from][i].to == vertex) {
                result.push_back(lists[from][i]);
            }
        }
    }

    return result;
}

struct PairComparatorOfLen {
    bool operator()(const std::pair<int, int> lhs, const std::pair<int, int> rhs) const {
        if (lhs.second == rhs.second) {
            return lhs.first < rhs.first;
        } else {
            return lhs.second < rhs.second;
        }
    }
};


bool Relax(std::vector<int> &minLengths, const int fromVertex, IGraph::ToLen &vertexToLen) {
    // if way to vertex is shorter update way and if it is not reverse way like a Graphs - 0; 0 - Graphs
    if (vertexToLen.to != fromVertex && minLengths[vertexToLen.to] > vertexToLen.len) {
        int tmpLength = minLengths[vertexToLen.to];
        minLengths[vertexToLen.to] = vertexToLen.len;

        // set old value to erase old one
        vertexToLen.len = tmpLength;

        return true;
    }
    return false;
}

int findTheMinimalTreeByPrim(const IGraph &graph, const int from) {
    // vector with lengthsMin to vertices
    std::vector<int> lengthsMin(graph.VertexCount());
    lengthsMin.assign(lengthsMin.size(), INT32_MAX);
    lengthsMin[from] = 0;

    std::vector<int> parents(graph.VertexCount());
    parents[from] = -1;

    std::vector<bool> visited(graph.VertexCount());
    visited.assign(visited.size(), false);
    visited[from] = true;


    // pair of <point, len to this point>
    std::set<std::pair<int, int>, PairComparatorOfLen> set;
    set.insert(std::pair<int, int>(from, lengthsMin[from]));

    while (!set.empty()) {
        // get the first element from set
        auto vertex = set.begin();

        std::vector<IGraph::ToLen> listVertexToLen = graph.GetNextVertices(vertex->first);
        for (int i = 0; i < listVertexToLen.size(); ++i) {
            if (lengthsMin[listVertexToLen[i].to] == INT32_MAX) {
                // if vertex first visited
                parents[listVertexToLen[i].to] = vertex->first;
                lengthsMin[listVertexToLen[i].to] = listVertexToLen[i].len;
                set.insert(std::pair<int, int>(listVertexToLen[i].to, lengthsMin[listVertexToLen[i].to]));
            } else if (!visited[listVertexToLen[i].to] && Relax(lengthsMin, vertex->first, listVertexToLen[i])) {
                // Decreasing key,
                // listVertexToLen was updated in Relax() by old value in set, to find it out and erase
                parents[listVertexToLen[i].to] = vertex->first;
                set.erase(std::pair<int, int>(listVertexToLen[i].to, listVertexToLen[i].len));
                set.insert(std::pair<int, int>(listVertexToLen[i].to, lengthsMin[listVertexToLen[i].to]));
            }
        }
        visited[vertex->first] = true;
        set.erase(vertex);
    }

    int weight = 0;
    for (int i = 0; i < lengthsMin.size(); ++i) {
        weight += lengthsMin[i];
    }
    return weight;
}

int main() {
//    test1();
//    test2();
//    test3();
//    test4();
//    test5();

    int n; // number of cities
    std::cin >> n;
    WeighedListGraph graph = WeighedListGraph(n);
    int m; // number of roads
    std::cin >> m;
    int from;
    int to;
    int len;
    for (int i = 0; i < m; ++i) {
        std::cin >> from >> to >> len;
        graph.AddEdge(from - 1, to - 1, len);
    }

    std::cout << findTheMinimalTreeByPrim(graph, 0);

    return 0;
}

/////////////////////////////////////////////////TESTS//////////////////////////////////////////////////////////////////

struct FromToLen {
    int from;
    int to;
    int len;

    FromToLen(int from, int to, int len) : from(from), to(to), len(len) {}
};

void test1() {
    std::vector<FromToLen> data = {{4, 3, 3046},
                                   {4, 5, 90110},
                                   {5, 1, 57786},
                                   {3, 2, 28280},
                                   {4, 3, 18010},
                                   {4, 5, 61367},
                                   {4, 1, 18811},
                                   {4, 2, 69898},
                                   {3, 5, 72518},
                                   {3, 1, 85838},};

    int n = 5;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from - 1, data[i].to - 1, data[i].len);
    }
    int from = 0;
    int result = findTheMinimalTreeByPrim(graph, from);
    if ( result == 107923) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED 107923 RESULT: " << result << "\n";
    }
}

void test2() {
    std::vector<FromToLen> data = {{0, 2, 2},
                                   {0, 3, 6},
                                   {0, 1, 9},
                                   {1, 6, 4},
                                   {2, 4, 1},
                                   {2, 3, 3},
                                   {3, 1, 2},
                                   {3, 6, 7},
                                   {3, 5, 9},
                                   {4, 3, 1},
                                   {4, 7, 6},
                                   {5, 6, 1},
                                   {5, 8, 1},
                                   {5, 7, 5},
                                   {7, 8, 5},};

    int n = 9;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from, data[i].to, data[i].len);
    }
    int from = 0;
    int result = findTheMinimalTreeByPrim(graph, from);
    if ( result == 17) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED 17 RESULT: " << result << "\n";
    }
}

void test3() {
    std::vector<FromToLen> data = {{0, 1, 5},
                                   {0, 2, 7},
                                   {1, 2, 9},
                                   {2, 3, 8},
                                   {2, 4, 7},
                                   {3, 4, 5},
                                   {1, 4, 15},
                                   {1, 5, 6},
                                   {5, 4, 8},
                                   {5, 6, 11},
                                   {4, 6, 9},};

    int n = 7;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from, data[i].to, data[i].len);
    }
    int from = 0;
    int result = findTheMinimalTreeByPrim(graph, from);
    if ( result == 39) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED 39 RESULT: " << result << "\n";
    }
}

void test4() {
    std::vector<FromToLen> data = {{0, 1, 5},
                                   {0, 2, 1},
                                   {0, 3, 5},
                                   {0, 4, 4},
                                   {0, 4, 1},
                                   {0, 5, 6},
                                   {1, 2, 6},
                                   {2, 3, 5},
                                   {3, 4, 2},
                                   {4, 0, 3},
                                   {4, 0, 2},
                                   {4, 5, 6},
                                   {5, 1, 3},};

    int n = 6;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from, data[i].to, data[i].len);
    }
    int from = 0;
    int result = findTheMinimalTreeByPrim(graph, from);
    if ( result == 12) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED 12 RESULT: " << result << "\n";
    }
}

void test5() {
    std::vector<FromToLen> data = {{0, 1, 4},
                                   {0, 2, 8},
                                   {1, 2, 11},
                                   {1, 3, 8},
                                   {2, 4, 7},
                                   {2, 5, 1},
                                   {4, 3, 2},
                                   {4, 5, 6},
                                   {5, 6, 2},
                                   {3, 6, 4},
                                   {3, 7, 7},
                                   {6, 7, 14},
                                   {6, 8, 10},
                                   {7, 8, 9}};

    int n = 9;
    int m = data.size();
    WeighedListGraph graph = WeighedListGraph(n);
    for (int i = 0; i < m; ++i) {
        graph.AddEdge(data[i].from, data[i].to, data[i].len);
    }
    int from = 0;
    int result = findTheMinimalTreeByPrim(graph, from);
    if ( result == 37) {
        std::cout << "OK\n";
    } else {
        std::cout << "EXPECTED 37 RESULT: " << result << "\n";
    }
}