/*
 * Написать алгоритм для решения игры в “пятнашки”.
 * Решением задачи является приведение к виду: [ Graphs BFS Dijkstra A-Star ] [ Prim 6 7 8 ] [ 9 10 11 12] [ 13 14 15 0 ],
 * где 0 задает пустую ячейку. Достаточно найти хотя бы какое-то решение.
 * Число перемещений костяшек не обязано быть минимальным.
 */

#include <array>
#include <queue>
#include <set>
#include <unordered_map>
#include <assert.h>
#include <string>
#include <cstring>
#include <iostream>
#include <algorithm>

using std::array;
using std::queue;
using std::unordered_map;
using std::swap;
using std::string;
using std::cout;
using std::ostream;

void test1();

void test2();

void test3();

const int SideSize = 4;
const int FieldSize = SideSize * SideSize;
const array<char, FieldSize> FinishField( { 1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7,
                                            8,
                                            9,
                                            10,
                                            11,
                                            12,
                                            13,
                                            14,
                                            15,
                                            0 } );

class GameState {
public:
    GameState( const array<char, FieldSize>& _field );

    bool CanMoveLeft() const;
    bool CanMoveUp() const;
    bool CanMoveRight() const;
    bool CanMoveDown() const;

    GameState MoveLeft() const;
    GameState MoveUp() const;
    GameState MoveRight() const;
    GameState MoveDown() const;

    bool IsFinish() const;

    bool operator == ( const GameState& state ) const { return field == state.field; }
    bool operator != ( const GameState& state ) const { return !(*this == state); }

    int getHeuristicOfGameField() const;

private:
    array<char, FieldSize> field;
    char zeroPos;

    friend ostream& operator << ( ostream& out, const GameState& state );
    friend struct StateHasher;
};

ostream& operator << ( ostream& out, const GameState& state )
{
    for( int y = 0; y < SideSize; ++y ) {
        for( int x = 0; x < SideSize; ++x ) {
            out << static_cast<int>(state.field[y * SideSize + x]) << " ";
        }
        out << std::endl;
    }
    return out;
}

GameState::GameState( const array<char, FieldSize>& _field ) :
        field( _field ),
        zeroPos( -1 )
{
    for( unsigned int i = 0; i < field.size(); ++i )
        if( field[i] == 0 ) {
            zeroPos = i;
            break;
        }
    assert( zeroPos != -1 );
}

bool GameState::CanMoveLeft() const
{
    return zeroPos % SideSize != 0;
}

bool GameState::CanMoveUp() const
{
    return zeroPos >= SideSize;
}

bool GameState::CanMoveRight() const
{
    return zeroPos % SideSize < SideSize - 1;
}

bool GameState::CanMoveDown() const
{
    return zeroPos < FieldSize - SideSize;
}

GameState GameState::MoveLeft() const
{
    assert( CanMoveLeft() );
    GameState newState( *this );
    swap( newState.field[newState.zeroPos], newState.field[newState.zeroPos - 1] );
    --newState.zeroPos;
    return newState;
}

GameState GameState::MoveUp() const
{
    assert( CanMoveUp() );
    GameState newState( *this );
    swap( newState.field[newState.zeroPos], newState.field[newState.zeroPos - SideSize] );
    newState.zeroPos -= SideSize;
    return newState;
}

GameState GameState::MoveRight() const
{
    assert( CanMoveRight() );
    GameState newState( *this );
    swap( newState.field[newState.zeroPos], newState.field[newState.zeroPos + 1] );
    ++newState.zeroPos;
    return newState;
}

GameState GameState::MoveDown() const
{
    assert( CanMoveDown() );
    GameState newState( *this );
    swap( newState.field[newState.zeroPos], newState.field[newState.zeroPos + SideSize] );
    newState.zeroPos += SideSize;
    return newState;
}

bool GameState::IsFinish() const
{
    return field == FinishField;
}

int GameState::getHeuristicOfGameField() const {
    int summaryHeuristicOfGameField = 0;
    for (int i = 0; i < FieldSize; ++i) {
        int val = field[i];
        // horizontal linear Heuristic, compare just prev element with current
        if (i % SideSize != 0 && i > 1 && val != 0) {
            if (((val - 1) / SideSize == (field[i - 1] - 1) / SideSize) &&
                    field[i - 1] > val) {
                summaryHeuristicOfGameField += 2;
            }
        }
        // vertical linear Heuristic, compare just on top of current element
        if (i > SideSize - 1 && val != 0) {
            if ((val - 1) % SideSize == (field[i - SideSize] - 1) % SideSize &&
                    field[i - SideSize] > val) {
                summaryHeuristicOfGameField += 2;
            }
        }
        // Manhattan Heuristic
        int dx = 0;
        int dy = 0;
        if (val != 0) {
            dx = abs(i % SideSize - (val - 1) % SideSize);
            dy = abs(i / SideSize - (val - 1) / SideSize);
        }
        summaryHeuristicOfGameField += dx + dy;
    }

    return summaryHeuristicOfGameField;
}

struct StateHasher {
    size_t operator()( const GameState& state ) const {
        size_t hash = 0;
        memcpy( &hash, &state.field[0], sizeof( hash ) );
        return hash;
    }
};

struct SetGameStateComparator {
    bool operator()(const std::pair<int, GameState>& lhs, const std::pair<int, GameState>& rhs) const {
        if (lhs.first == rhs.first) {
            return true;
        } else {
            return lhs.first < rhs.first;
        }
    }
};



string Get8thSolution( const GameState& state )
{
    // make set with summary int size Heuristic and Game State
    std::set<std::pair<int, GameState>, SetGameStateComparator> set;
    set.insert(std::pair<int, GameState> (state.getHeuristicOfGameField(), state));
    unordered_map<GameState, char, StateHasher> visited;
    visited[state] = 'S';
    bool hasSolution = false;
    while( !set.empty() ) {
        auto tempState = set.begin();

        if( tempState->second.IsFinish() ) {
            hasSolution = true;
            break;
        }
        if( tempState->second.CanMoveLeft() ) {
            GameState newState = tempState->second.MoveLeft();
            if( visited.find( newState ) == visited.end() ) {
                visited[newState] = 'L';
                set.insert( std::pair<int, GameState>(newState.getHeuristicOfGameField(), newState));
            }
        }
        if( tempState->second.CanMoveUp() ) {
            GameState newState = tempState->second.MoveUp();
            if( visited.find( newState ) == visited.end() ) {
                visited[newState] = 'U';
                set.insert( std::pair<int, GameState>(newState.getHeuristicOfGameField(), newState));
            }
        }
        if( tempState->second.CanMoveRight() ) {
            GameState newState = tempState->second.MoveRight();
            if( visited.find( newState ) == visited.end() ) {
                visited[newState] = 'R';
                set.insert( std::pair<int, GameState>(newState.getHeuristicOfGameField(), newState));
            }
        }
        if( tempState->second.CanMoveDown() ) {
            GameState newState = tempState->second.MoveDown();
            if( visited.find( newState ) == visited.end() ) {
                visited[newState] = 'D';
                set.insert( std::pair<int, GameState>(newState.getHeuristicOfGameField(), newState));
            }
        }
        set.erase(tempState);
    }

    if( !hasSolution )
        return string();

    string result;
    GameState tempState( FinishField );
    char move = visited[tempState];
    while( move != 'S' ) {
        result += move;
        switch( move ) {
            case 'L':
                tempState = tempState.MoveRight();
                break;
            case 'U':
                tempState = tempState.MoveDown();
                break;
            case 'R':
                tempState = tempState.MoveLeft();
                break;
            case 'D':
                tempState = tempState.MoveUp();
                break;
        }
        move = visited[tempState];
    }
    std::reverse( result.begin(), result.end() );
    return result;
}

// function to prepare Solution for cout
std::vector<char> prepareSolution(GameState& state, std::string& solution) {
    std::vector<char> result;
    for( char direction : solution ) {
        switch( direction ) {
            case 'L':
                state = state.MoveLeft();
                result.push_back('R');
                break;
            case 'U':
                state = state.MoveUp();
                result.push_back('D');
                break;
            case 'R':
                state = state.MoveRight();
                result.push_back('L');
                break;
            case 'D':
                state = state.MoveDown();
                result.push_back('U');
                break;
        }
    }
    return result;
}

int main() {
    bool testing = true;
    if (!testing) {
        std::array<char, FieldSize> field = std::array<char, FieldSize>();
        int val;
        for (int i = 0; i < FieldSize; ++i) {
            std::cin >> val;
            field[i] = val;
        }
        GameState state(field);
        string solution = Get8thSolution(state);
        std::vector<char> result = prepareSolution(state, solution);

        std::cout << result.size() << "\n";
        for (char j: result) {
            std::cout << j;
        }
    } else {
        test1();
        test2();
//        test3();
    }
    return 0;
}



void test1() {
    std::array<char, FieldSize> field = { 1,
                                          2,
                                          3,
                                          4,
                                          5,
                                          6,
                                          7,
                                          8,
                                          9,
                                          10,
                                          11,
                                          0,
                                          13,
                                          14,
                                          15,
                                          12};
    GameState state(field);
    string solution = Get8thSolution(state);
    std::vector<char> result = prepareSolution(state, solution);

    std::cout << "TEST Graphs:\n";
    std::cout << result.size() << "\n";
    if (result.size() == 1) {
        for (char j: result) {
            std::cout << j;
        }
        std::cout << "\nOK\n";
    } else {
        std::cout << "FAIL, EXPECTED RESULT Graphs, RESULT: " << result.size() << "\n";
    }
}

void test2() {
    std::array<char, FieldSize> field = { 1,
                                          2,
                                          3,
                                          0,
                                          5,
                                          6,
                                          7,
                                          4,
                                          9,
                                          10,
                                          11,
                                          8,
                                          13,
                                          14,
                                          15,
                                          12};
    GameState state(field);
    string solution = Get8thSolution(state);
    std::vector<char> result = prepareSolution(state, solution);

    std::cout << "TEST BFS:\n";
    std::cout << result.size() << "\n";
    if (result.size() == 3) {
        for (char j: result) {
            std::cout << j;
        }
        std::cout << "\nOK\n";
    } else {
        std::cout << "FAIL, EXPECTED RESULT Dijkstra, RESULT: " << result.size() << "\n";
    }
}

void test3() {
    std::array<char, FieldSize> field = { 15,
                                          14,
                                          13,
                                          12,
                                          11,
                                          10,
                                          9,
                                          8,
                                          7,
                                          6,
                                          5,
                                          4,
                                          3,
                                          2,
                                          1,
                                          0};
    GameState state(field);
    string solution = Get8thSolution(state);
    std::vector<char> result = prepareSolution(state, solution);

    std::cout << "TEST Dijkstra:\n";
    std::cout << result.size() << "\n";
    if (result.size() == 3) {
        for (char j: result) {
            std::cout << j;
        }
        std::cout << "\nOK\n";
    } else {
        std::cout << "FAIL, EXPECTED RESULT Dijkstra, RESULT: " << result.size() << "\n";
    }
}
